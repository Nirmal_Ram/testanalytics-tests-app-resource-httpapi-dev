"use strict";
const calculateTimingKpi = require("./utils/calculateTimingKpi.js");
const calculateSize = require("./utils/calculateSize.js");
const countAssets = require("./utils/countAssets.js");
const filterResult = require("./utils/filterResult.js");
module.exports.parser = async (data) => {
  var JSONFormatter = JSON.parse(data);
  const topAsset = filterResult.calc(
    JSONFormatter.log.entries,
    "contentSize",
    null
  );
  const mostUpload = filterResult.calc(
    JSONFormatter.log.entries,
    "method",
    "POST"
  );
  const mostDownload = filterResult.calc(
    JSONFormatter.log.entries,
    "method",
    "GET"
  );
  const uniqueDomain = filterResult.calc(
    JSONFormatter.log.entries,
    "unique",
    null
  );
  const topDomains = filterResult.calc(
    JSONFormatter.log.entries,
    "topdomain",
    null
  );
  console.log(mostUpload)
  console.log(mostDownload)
  console.log(uniqueDomain)
  console.log(topDomains)
  const response = {
    harKpi: {
      har_dataexchange: {
        most_upload: {
          url: ((mostUpload.length!==0)?mostUpload[0].request.url:"No URL Available"),
          upload:( (mostUpload.length!==0)?Number(
            (Number(mostUpload[0].response.content.size) / 1000).toFixed(2)
          ):0),
          name: "UPLOAD",
          unit: "KB",
        },
        most_download: {
          url: mostDownload[0].request.url,
          download: Number(
            (Number(mostDownload[0].response.content.size) / 1000).toFixed(2)
          ),
          name: "DOWNLOAD",
          unit: "KB",
        },
      },
      har_assets: {
        labels: ["html/text", "js", "css", "image", "flash", "others"],
        values: uniqueDomain.map((domain) => {
          var urlBasedData = filterResult.calc(
            JSONFormatter.log.entries,
            "url",
            new URL(domain.request.url).host
          );
          return {
            url: new URL(domain.request.url).host,
            values: [
              Number(countAssets.calc(urlBasedData, "text/html").toFixed(2)),
              Number(
                countAssets.calc(urlBasedData, "text/javascript").toFixed(2)
              ),
              Number(countAssets.calc(urlBasedData, "text/css").toFixed(2)),
              Number(countAssets.calc(urlBasedData, "image/png").toFixed(2)),
              Number(
                countAssets
                  .calc(urlBasedData, "application/x-shockwave-flash")
                  .toFixed(2)
              ),
              Number(countAssets.calc(urlBasedData, "others").toFixed(2)),
            ],
          };
        }),
        name: "Assets",
        unit: "KB",
      },
      har_timingKpi: {
        labels: ["ssl", "connect", "send", "wait", "receive"],
        unit: "ms",
        values: uniqueDomain.map((domain) => {
          var urlBasedData = filterResult.calc(
            JSONFormatter.log.entries,
            "url",
            new URL(domain.request.url).host
          );
          return {
            url: new URL(domain.request.url).host,
            values: [
              Number(calculateTimingKpi.calc(urlBasedData, "ssl").toFixed(2)),
              Number(
                calculateTimingKpi.calc(urlBasedData, "connect").toFixed(2)
              ),
              Number(calculateTimingKpi.calc(urlBasedData, "send").toFixed(2)),
              Number(calculateTimingKpi.calc(urlBasedData, "wait").toFixed(2)),
              Number(
                calculateTimingKpi.calc(urlBasedData, "receive").toFixed(2)
              ),
            ],
          };
        }),
      },
      har_topAssets: topAsset.slice(0, 3).map((data) => {
        return {
          url: data.request.url,
          type: data.response.content.mimeType,
          size: data.response.content.size,
          unit: "B",
        };
      }),
      har_topRequests: topDomains.slice(0, 3).map((data) => {
        return {
          url: data,
          type: filterResult.calc(JSONFormatter.log.entries, "url", data)[0]
            .response.content.mimeType,
          size: calculateSize.calc(
            filterResult.calc(JSONFormatter.log.entries, "url", data)
          ),
          unit: "B",
        };
      }),
    },
  };
  return response;
};

"use strict";
module.exports.calc = (arr , mimeType) => {
    let result = 0;
    if (arr.length != 0 && mimeType != "others") {
      arr.map((data) => {
        if (data.response.content.mimeType === mimeType) {
          result += Number(data.response.content.size);
        }
      });
    } else if (arr.length != 0 && mimeType === "others") {
      arr.map((data) => {
        if (
          data.response.content.mimeType != "text/html" &&
          data.response.content.mimeType != "text/javascript" &&
          data.response.content.mimeType != "text/css" &&
          data.response.content.mimeType != "image/png" &&
          data.response.content.mimeType != "application/x-shockwave-flash"
        ) {
          result += Number(data.response.content.size);
        }
      });
    }
    return result;
  };
"use strict";
module.exports.calc = (arr, entity, value) => {
  var result = [];
  if (entity === "url") {
    result = arr.filter((data) => {
      return new URL(data.request.url).host === value;
    });
  } else if (entity === "contentSize") {
    result = arr.sort((A, B) => {
      return B.response.content.size - A.response.content.size;
    });
  } else if (entity === "method") {
    let tempArray = arr.filter((data) => {
      return data.request.method === value;
    });
    result = tempArray.sort((A, B) => {
      return B.response.content.size - A.response.content.size;
    });
  } else if (entity === "unique") {
    result = [
      ...new Map(
        arr.map((item) => [new URL(item.request.url).host, item])
      ).values(),
    ];
  } else if (entity === "topdomain") {
    let hashmap = new Map();
    arr.map((url) => {
      if (hashmap.has(new URL(url.request.url).host)) {
        return hashmap.set(
          new URL(url.request.url).host,
          hashmap.get(new URL(url.request.url).host) + 1
        );
      } else {
        return hashmap.set(new URL(url.request.url).host, 1);
      }
    });
    let sortMap = new Map([...hashmap.entries()].sort((a, b) => b[1] - a[1]));
    let list = [...sortMap];
    list.slice(0, 4).map((itm) => {
      result.push(itm[0]);
    });
  }
  return result;
};

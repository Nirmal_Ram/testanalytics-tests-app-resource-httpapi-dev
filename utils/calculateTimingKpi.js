"use strict";
module.exports.calc = (arr, kpiName) => {
    let result = 0;
    if (arr.length != 0) {
        arr.map((itm) => {
          result += Number(itm.timings[kpiName]);
        });
      }
    return result;
  };
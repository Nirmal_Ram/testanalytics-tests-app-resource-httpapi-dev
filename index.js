"use strict";
const dbjs = require("./db.js");
const har = require("./harParser.js");
const HARparser = require("./parser.js");

exports.handler = async (event) => {
  const db = await dbjs.get();
  const id = event.params.path.testUUID;
  const exists = await db
    .collection("hotstar-testAnalytics")
    .find({ "uuid._id": id, appKpi: { $exists: true } })
    .toArray();
  //check whether HAR_KPI field is available or Not
  if (exists.length == 0) {
    const url = await db
      .collection("hotstar-testAnalytics")
      .find({ "uuid._id": id })
      .project({
       info: 1,
      })
      .toArray();
    if (
      url.length !== 0 &&
      url[0].info.testOutputArtifacts.harMetrics.url != undefined &&
      url[0].info.testOutputArtifacts.harMetrics.url != ""
    ) {
      let HARdata = await HARparser.parser(
        url[0].info.testOutputArtifacts.harMetrics.url
      );
      let result = await har.parser(HARdata);
      const post = await db
        .collection("hotstar-testAnalytics")
        .updateOne({ "uuid._id": id }, { $set: {"info.appKpi":result} });
    }
  }
  const data = await db
  .collection("hotstar-testAnalytics")
  .findOne({ "uuid._id": event.params.path.testUUID }, { uuid: 1, info: 1 });
  var url;
  if(data.info.testOutputArtifacts.hasOwnProperty("harMetrics")){
    url = data.info.testOutputArtifacts.harMetrics.url;
  }
  else{
    url=""
  }
  const result = {
    version: "1.2",
    uuid: {
      _id: data.uuid._id,
      testId: data.uuid.testId,
      sessionId: data.uuid.sessionId,
      userDisplayName: data.uuid.userDisplayName,
      userEmail: data.uuid.userEmail,
      username: data.uuid.username,
    },
    datas: [
      {
        source: "link",
        url: url,
      },
    ],
    kpi: data.info.appKpi.harKpi,
  };
  const response = {
    statusCode: 200,
    body: result,
    headers: {
      "Access-Control-Allow-Headers":
        "Content-Type, Origin, X-Requested-With, Accept, Authorization, Access-Control-Allow-Methods, Access-Control-Allow-Headers, Access-Control-Allow-Origin",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "OPTIONS,POST,GET,PUT",
    },
  };
  return response;
};

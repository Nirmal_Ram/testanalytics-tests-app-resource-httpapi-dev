"use strict";
var fetch = require("node-fetch");
module.exports.parser = async (url) => {
    const response = await fetch(url, { method: "GET" });
    const data = await response.text();
    return data;
  };